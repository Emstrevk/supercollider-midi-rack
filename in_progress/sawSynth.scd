/* Retrieved from
http://sccode.org/1-5aD
*/

SynthDef("sawSynth", { arg freq = 440, amp = 1.00, att = 0.1, rel = 2, lofreq = 1000, hifreq = 3000;
    var env, snd;
	amp = amp * 0.8;
    env = Env.perc(
		attackTime: att,
		releaseTime: rel,
		level: amp
	).kr(doneAction: 0);
    snd = Saw.ar(freq: freq * [0.99, 1, 1.001, 1.008], mul: env);
	snd = LPF.ar(
		in: snd,
		freq: LFNoise2.kr(1).range(lofreq, hifreq)
	);
    snd = Splay.ar(snd);

	snd = FreeVerb.ar(snd, mix: 0.4, room: 1.0, damp: 0.4, mul: 0.6, add: 0.0);
    Out.ar(0, snd);
// Basic saw synth for chords and bass
//By Bruno Ruviaro
//http://sccode.org/1-54H
}).add;
