# SuperCollider MIDI Rack
### How to run
- python main.py should do it. Dependency list is sadly not up to date currently so install as necessary.
- Only guaranteed to work in latest python version (3.4+), use venv if possible
### Why
1. Because easily configured, non-gui digital synthesizers with straightforward, named, and unique MIDI input connections are few and far in between.
2. Because the bare minimums of supercollider OSC control are tricky to learn in the current google-landscape
### Intended use case:
1. User creates a folder of custom synth defs in .scd format
2. Application starts supercollider and loads the synthdefs 
3. Application creates a synth for each definition and attaches a virtual MIDI input port to each of them
4. User can now connect any MIDI device to any of the specified synth defs and play them as if they were regular MIDI synths
### Tech
The application will first and foremost be constructed using bare minimum python wrappers around native OSC messages.

This both to simplify later porting and provide a working example of "basic" OSC control of supercollider synths.

#### Inspirations (and synthDefs!) taken from:
- https://github.com/ideoforms/python-supercollider
- https://github.com/Qirky/FoxDot
- https://github.com/SCLOrkHub/SCLOrkSynths
#### Dependency outline at the time of writing:
- liblo (For server connection and message sending)
- python-rtmidi (For midi input connection callbacks)
- supercollider/sclang
- sc3-plugins needed for some synthdefs
- jack
