import os.path
from pathlib import Path
from typing import Dict, List

SYNTHDEF_PATH = "synths"
KIT_PATH = "kits"
SYNTHDEF_EXTENSION = "*.scd"

class CombinedScdData():
    def __init__(self):
        self.merged_scd_code = ""
        self.contained_synth_names = []
        self.contained_synth_kits: Dict[str, List[str]] = {}


def read_scd_files() -> CombinedScdData:

    result = CombinedScdData()
    # Create a master synth file of combined data for faster loading
    for synth_file in Path(SYNTHDEF_PATH).rglob(SYNTHDEF_EXTENSION):

        with open(synth_file, 'r') as fp:

            lines = fp.read().split("\n")

            for line in lines:
                result.merged_scd_code += "\n"
                result.merged_scd_code += line.strip()
        
        # Example: synths/paperSynth.scd -> paperSynth
        base_name = str(synth_file).split("/")[-1].split(".scd")[0]
        
        result.contained_synth_names.append(base_name)

    for synth_file in Path(KIT_PATH).rglob(SYNTHDEF_EXTENSION):

        with open(synth_file, 'r') as fp:

            lines = fp.read().split("\n")

            for line in lines:
                result.merged_scd_code += "\n"
                result.merged_scd_code += line.strip()
        
        # Example: synths/paperSynth.scd -> paperSynth
        base_name = str(synth_file).split("/")[-1].split(".scd")[0]
        
        # For some reason dirname outputs the whole thing from /kits/
        # THis is also bug prone, don't rely on it TODO
        kit_name = os.path.dirname(synth_file).split("/")[1]

        # Kit synths are saved with a reference to their parent
        # e.g. WHen you call "kick" in a drumset your don't refer to "/kick" but to "/myDrumsSet tone n" which in turn redirects to "/kick"
        if kit_name not in result.contained_synth_kits:
            result.contained_synth_kits[kit_name] = []

        result.contained_synth_kits[kit_name].append(base_name)
    
    # TODO: Kit path; need to know subfolder name etc.

    return result



    