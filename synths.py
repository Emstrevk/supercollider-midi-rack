import liblo
from rtmidi.midiconstants import (BANK_SELECT_LSB, BANK_SELECT_MSB, CHANNEL_PRESSURE,
                                  CONTROLLER_CHANGE, NOTE_ON, NOTE_OFF, ALL_NOTES_OFF, PROGRAM_CHANGE)
from pretty_midi.utilities import note_number_to_hz
import time
from rtmidi.midiutil import open_midiinput

from random import randint
from typing import List

# Incrementer object to be shared between e.g. synth objects so that node ids remain unique
class SharedIncrementer:
    def __init__(self):
        self.id = 10 # Start at ten in case supercollider has created other nodes early

    def get_next(self):
        self.id += 1
        return self.id


'''
    More advanced mapping where a single port-name maps to several underlying synth objects,
    e.g. when you want a drum kit which encompasses more than one synth mapped to different tones

    TODO: Lots of duplicate code, should be rather easy to refactor into some kind of composition pattern
'''
class ScMIDIKit(object):
    def __init__(self, kit_name: str, synths: List[str], note_id_incrementer: SharedIncrementer, server_address: liblo.Address):
        midiin, port_name = open_midiinput(None, use_virtual=True, port_name=kit_name)
        midiin.set_callback(self)

        self.kit_name = kit_name
        self.incrementer = note_id_incrementer
        self.server_address = server_address

        # regular synth map can be re-used: Same principle as before where each tone can have many running s_new
        #   BUT instead of calling s_new on a static string name we fetch the correct underlying synth each time
        self.synth_map = {} # Maps <MidiToneIndex,list<nodeIndex>>
        self.synth_def_list = synths

        print("Created kit on port: " + kit_name)

        # Make sure synth amount is divisible by 12
        # This to ensure that for example tone c and tone c2 target the same instrument (there are 12 semitones in an octave)
        while len(self.synth_def_list) < 12:
            self.synth_def_list.append(self.synth_def_list[-1])

        # List of note-off registered synths to clean when appropriate
        # (killing them without first letting them go quiet results in pops)
        self.cleaup_ids = []

    def _tone_to_synth(self, tone: int) -> str:
        # Looparound if too high
        if tone > len(self.synth_def_list)-1:
            # Should follow pattern: last_index(5) index(33) => final_index(3)
            # 33 / 5 = 6.6 -> 6 * 5 = 30 + 3
            # Bug warning: int() may produce round-up after 16 decimals
            # 29:7 -> 29 / 7 = 4 -> 4 * 7 = 28, 29-28 = 1
            floored_division = int(tone/(len(self.synth_def_list)-1))
            divisible_amount = floored_division * ( len(self.synth_def_list)-1 )
            remaining_after_divide = tone - divisible_amount
            return self.synth_def_list[remaining_after_divide]
        else:
            return self.synth_def_list[tone]

    def __call__(self, event, data=None):

        message, _ = event
        #print("Hi I was called: " + str(message))
        msg_type = message[0] & 0xF0

        if msg_type == 0x70:
            print("PANIC received")
            for key in self.synth_map:
                for tone_id in self.synth_map[key]:
                    liblo.send(self.server_address, "n_free", tone_id)
            self.synth_map = {}

        if msg_type in (NOTE_ON, NOTE_OFF):
            tone: int = message[1]
            if (msg_type == NOTE_ON):
                velocity: float = message[2] / 127
                freq: float = note_number_to_hz(tone)
                new_id: int = self.incrementer.get_next()
                if (velocity > 0):
                    liblo.send(
                        self.server_address,
                        "/s_new",
                        self._tone_to_synth(tone), # Pretty much only difference in code vs regular synth call
                        new_id,
                        0,0,
                        "freq", freq,
                        "amp", velocity
                    )

                    # Make sure the list can be populated
                    if tone not in self.synth_map:
                        self.synth_map[tone] = []

                    self.synth_map[tone].append(new_id)
                #print("NOTE_ON: " + str(tone) + ", " + str(velocity))
            else:

                # Perform a bit of cleanup on regular intervals
                # Could be anywhere but I put it here for convenience
                # As a result the very last note off never gets cleaned (memory leak?) but that's ok for now
                for garbage in self.cleaup_ids:
                    liblo.send(self.server_address, "n_free", garbage)

                self.cleaup_ids = []

                # Remove oldest element, this is a bit bug prone since the oldest
                #   tone is not necessarily the latest note_off event
                if tone in self.synth_map:
                    if len(self.synth_map[tone]) > 0:
                        existing_id = self.synth_map[tone].pop(0)
                        liblo.send(self.server_address, "n_set", existing_id, "freq", 0.0)
                        self.cleaup_ids.append(existing_id)




'''
    Basic MIDI Synth callback object.
    Attaches as callback to an RTMIDI Input so that it can read MIDI events
    Each NOTE_ON event will trigger an s_new (new synth object creation) in
    the given tone frequency, while NOTE_OFF performs abrupt cleanup by freeing
    these synths by their assigned id.
'''
class ScMIDISynth(object):
    def __init__(self, synth_name: str, node_id_incrementer: SharedIncrementer, server_address: liblo.Address):


        midiin, _ = open_midiinput(None, use_virtual=True, port_name=synth_name)
        midiin.set_callback(self)

        self.synth_name = synth_name
        self.incrementer = node_id_incrementer
        self.server_address = server_address
        self.synth_map = {} # Maps <MidiToneIndex,list<nodeIndex>>

        print("Created synth on port: " + synth_name)

    def __call__(self, event, data=None):

        message, _ = event
        #print("Hi I was called: " + str(message))
        msg_type = message[0] & 0xF0

        if msg_type == 0x70: # TODO: Actually 0x7B but jackdaw server is sending wrong
            print("PANIC received")
            for key in self.synth_map:
                for tone_id in self.synth_map[key]:
                    liblo.send(self.server_address, "n_free", tone_id)
            self.synth_map = {}
        elif msg_type in (NOTE_ON, NOTE_OFF):
            tone: int = message[1]
            if (msg_type == NOTE_ON):
                velocity: float = message[2] / 127
                freq: float = note_number_to_hz(tone)
                new_id: int = self.incrementer.get_next()
                liblo.send(
                    self.server_address,
                    "/s_new",
                    self.synth_name,
                    new_id,
                    0,0,
                    "freq", freq,
                    "amp", velocity
                )

                # Make sure the list can be populated
                if tone not in self.synth_map:
                    self.synth_map[tone] = []

                self.synth_map[tone].append(new_id)
                #print("NOTE_ON: " + str(tone) + ", " + str(velocity))
            else:
                # Remove oldest element, this is a bit bug prone since the oldest
                #   tone is not necessarily the latest note_off event
                if tone in self.synth_map:
                    if len(self.synth_map[tone]) > 0:
                        existing_id = self.synth_map[tone].pop(0)
                        liblo.send(self.server_address, "n_free", existing_id)
        else:
            print(str(message) + " " + str(msg_type))
