import sys # For handling exit etc.
import subprocess # For starting sclang
import liblo # For calling client scd load method
import os # For setting rtmidi backend env variable
from pathlib import Path # For scanning synthDef dirs 
import time # For sleep() 
from typing import List # Strict typing on kit return
from curtsies import Input  # For keypress detection (live reload)

from synths import ScMIDISynth, SharedIncrementer, ScMIDIKit
from scd_lib import CombinedScdData, read_scd_files 


# Config contstants; todo: port to config file
SCLANG_EXEC = "sclang"
STARTUP_FILE = "startup.scd"
MIDI_API = "UNIX_JACK" # Currently only api supported; expand to enum when more options are available 
CLIENT_PORT = 57120 # Arbitrary port sure not to be taken
SERVER_PORT = 57110 # Default used by supercollider
SERVER_BOOT_WAIT_SECONDS = 3 # In place of a true callback we just wait an arbitrary amount of seconds before starting operations
SERVER_IP = "127.0.0.1" # Typically localhost unless you're trying to be a hero of some kind

MSG_CUSTOM_SCD_LOAD = "/customload" # Name/address of the function defined in startup.scd used to load synthDefs in native supercollider client

def debug(message: str) -> None:
    print("DEBUG: " + message)

def run():

    os.environ["RTMIDI_API"] = MIDI_API # Read during open_midiinput

    # Launch supercollider
    daemon = subprocess.Popen([SCLANG_EXEC, STARTUP_FILE, "-u", str(CLIENT_PORT)])
    
    debug("Waiting for server to become ready...")
    time.sleep(SERVER_BOOT_WAIT_SECONDS)
    
    # Establish connection
    hostname = SERVER_IP
    client_address = liblo.Address(hostname, CLIENT_PORT)
    server_address = liblo.Address(hostname, SERVER_PORT)

    shared_incrementer = SharedIncrementer()

    def create_synth(synth_name: str):
        try:
            # Constructor attaches itself to an input port; no need to save variable elsewhere
            ScMIDISynth(
                synth_name,
                shared_incrementer,
                server_address)

        except (EOFError, KeyboardInterrupt):
            sys.exit()

    def create_kit(kit_name: str, kit_synths: List[str]):
        try:
            # Constructor attaches itself to an input port; no need to save variable elsewhere
            ScMIDIKit(
                kit_name,
                kit_synths,
                shared_incrementer,
                server_address)

        except (EOFError, KeyboardInterrupt):
            sys.exit()

    def read_synthsdefs() -> CombinedScdData:
        scd_data: CombinedScdData = read_scd_files()
        combined_file_data = scd_data.merged_scd_code

        #combined_file_data += "\n(\"CLIENT: All synth:load calls finished\").postln;\n"
        with open("temp.scd", 'w') as combined_file:
            combined_file.write(combined_file_data)
    
        #debug("Attempting to load: merged file temp.scd, if error, validate that client port hasn't auto-incremented due to dead client")
        liblo.send(client_address, MSG_CUSTOM_SCD_LOAD, "temp.scd")

        return scd_data

    scd_data = read_synthsdefs()

    debug("Starting MIDI port Creation...")

    for synth in scd_data.contained_synth_names:
        create_synth(synth)

    for kit, synth in scd_data.contained_synth_kits.items():
        create_kit(kit, synth)

    print("Done!")
    print("Ctrl+c to exit, press 'r' for live reload (Update: nope, happens every sec)")
    try:
        while True:
            #with Input(keynames='curses') as input_generator:
                #for e in input_generator:
                    #if (str(e) == "r"):
                        #read_synthsdefs()
                        #print("reloaded synths!")
                    #else:
                        #print(repr(e) + ":" + str(e))
            time.sleep(1)
            read_synthsdefs()
           
    except KeyboardInterrupt:
        debug("Shutting down!")
        liblo.send(client_address, "/quit")
        liblo.send(server_address, "/quit")
        time.sleep(1)
        daemon.terminate()

# Program starts here
try:
    run()
except KeyboardInterrupt:
    print("Received keyboard interrupt")
        # Shutdown:
